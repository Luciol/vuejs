new Vue({
    el: "#app",
    data: {
        show: true,
        playerHealth: 100,
        monsterHealth : 100,
        log: []
    },
    methods: {
        getRandomInt: function(max) {
            return Math.floor(Math.random() * Math.floor(max));
        },
        monsterAttack: function() {
            var damage = this.getRandomInt(20)
            this.playerHealth -= damage;
            this.logger('Monster', damage)
        },
        playerAttack: function(power) {
            var playerDamage = this.getRandomInt(power);
            this.monsterHealth -= playerDamage;
            this.logger('Player', playerDamage)
            this.monsterAttack();
        },
        heal: function() {
            var recovery = this.getRandomInt(20)
            this.playerHealth += recovery;
            if(this.playerHealth > 100) {
                this.playerHealth = 100;
            }
            this.logger('Player', recovery, 'heal')
            this.monsterAttack();
        }, 
        logger: function(player, amount, action = 'attack') {
            if (action === 'heal') {
                var msg = `${player} heals for ${amount}`
            } else {
                msg = `${player} hit for ${amount}`
            }
            this.log.push(msg);
        },
        endGame: function(msg = 'Do you want to give up ?') {
            if (window.confirm(msg)) {
                this.reset()
            }
        },
        reset: function() {
            this.playerHealth = 100;
            this.monsterHealth = 100;
            this.show = true;
            this.log = [];
        }
    },
    computed: {
        playerBar: function () {
            return this.playerHealth < 0 ? this.endGame('You lose ! Do you want to start a new game ?') : this.playerHealth + '%';
        },
        monsterBar: function () {
            return this.monsterHealth < 0 ? this.endGame('You win ! Do you want to start a new game ?') : this.monsterHealth + '%';
        }
    }
})