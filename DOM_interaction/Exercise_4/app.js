new Vue({
	el: "#exercise",
	data: {
		isActive : false,
		text: 'text',
		textColor: 'textColor',
		background: '',
		anotherBackground: '',
		activeClass: false,
		width: 100,
		progress: 0
	},
	methods: {
		startEffect: function() {
			vm = this;
			setInterval(() => {
				return vm.isActive = !vm.isActive;
			}, 2000);
		},
		progressBar: function() {
			if(this.progress < 100) {
				this.progress += 5;
			}
		}
	},
	computed: {
		addColor: function() {
			return this.background;
		},
		attachClass: function() {
			return this.anotherBackground;
		},
		properties: function() {
			return {
				height: this.width + 'px',
				width: this.width + 'px',
				backgroundColor: 'blue'
			}
		}
	}
})
