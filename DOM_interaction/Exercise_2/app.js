new Vue({
    el: '#exercise',
    data: {
        value: ''
    },
    methods: {
        showAlert: function () {
            alert('Button clicked');
        },
        keyValue: function (event) {
            this.value = event.key;
        }
    }
});