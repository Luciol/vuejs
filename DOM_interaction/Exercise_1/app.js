new Vue({
    el: '#exercise',
    data: {
        name: 'Loic', 
        age: '42',
        source: 'https://www.assimil.com/blog/wp-content/uploads/2018/06/AdobeStock_55386318-2.jpeg'
    },
    methods: {
        /**
         * @param {number} multiplier 
         */
        multipleAgeBy: function (multiplier) {
            return this.age * multiplier;
        },
        randomFloat: function() {
            return Math.random();
        }
    }
})