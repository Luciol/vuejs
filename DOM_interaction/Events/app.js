new Vue({
    el: '#app',
    data: {
        counter: 0,
        x: 0, 
        y: 0,
        name: 'Loïc'
    },
    methods: {
        count: function (number) {
            this.counter += number;
        },
        updateCoordinates: function (event) {
            this.x = event.clientX;
            this.y = event.clientY;
        },
        alertMe: function () {
            alert('Alert');
        }
    }
});
